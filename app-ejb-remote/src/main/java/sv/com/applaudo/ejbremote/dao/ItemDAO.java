/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.applaudo.ejbremote.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import sv.com.applaudo.ejbremote.models.entities.Item;
import sv.com.applaudo.ejbremote.models.enums.EnumStatus;

/**
 *
 * @author stevo
 */
public class ItemDAO {
    
     private EntityManager entityManager;
    
    
    public ItemDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    public Item findById(Long id) {
        Item item = entityManager.find(Item.class, id);
        return item != null ? item : null;
    }
    
    public List<Item> findAll() {
        String consulta = "Item.findAll";
        return entityManager.createNamedQuery(consulta).getResultList();
    }
    
    public List<Item> findByStatusAndName(EnumStatus status, String name) throws Exception {
        
        List<Item> listaItems = new ArrayList();
        
        try{
        
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();     

            CriteriaQuery<Item> consulta = cb.createQuery(Item.class);
            Root<Item> items = consulta.from(Item.class);
            Predicate p1 = null,p2 = null;

            //Buscar por el nombre del item case insensitive
            if(name!=null){
                p1=cb.like(cb.lower(items.get("itemName")), "%" + name.toLowerCase() + "%");
            }
            //Buscar por el status, igual a 
            if(status!=null){
                p2= cb.equal(items.get("itemStatus"),status) ;
            }

            Predicate nameAndStatus=cb.and(p1,p2);

            consulta.select(items).where(nameAndStatus);

            listaItems = entityManager.createQuery(consulta).getResultList();

            return listaItems;
        }
        catch(Exception e){
            throw e;
        }
    }

    public Item save(Item item) throws Exception {
        try {
            entityManager.persist(item);
            return item;
        } catch (Exception e) {
            throw e;
        }
    }
    
     public Map<String, Object> filterData(String filter, Long id) {

        if (filter == null)
            return Collections.EMPTY_MAP;

        Item item = findById(id);
        if (item == null)
            return Collections.EMPTY_MAP;

        Map<String, Object> map = new HashMap<>();

        String[] fields = filter.split(",");

        for (String field : fields) {
            if (field.equalsIgnoreCase("itemId")) {
                map.put("itemId", item.getItemId());
            }

            if (field.equalsIgnoreCase("itemName")) {
                map.put("itemName", item.getItemName());
            }

            if (field.equalsIgnoreCase("itemBuyingPrice")) {
                map.put("itemBuyingPrice", item.getItemBuyingPrice());
            }

            if (field.equalsIgnoreCase("itemSellingPrice")) {
                map.put("itemSellingPrice", item.getItemSellingPrice());
            }
            
            if (field.equalsIgnoreCase("itemEnteredByUser")) {
                map.put("itemEnteredByUser", item.getItemEnteredByUser());
            }

            if (field.equalsIgnoreCase("itemEnteredDate")) {
                map.put("itemEnteredDate", item.getItemEnteredDate());
            }
            
            if (field.equalsIgnoreCase("itemLastModifiedDate")) {
                map.put("itemLastModifiedDate", item.getItemLastModifiedDate());
            }
            
            if (field.equalsIgnoreCase("itemLastModifiedByUser")) {
                map.put("itemLastModifiedByUser", item.getItemLastModifiedByUser());
            }

            if (field.equalsIgnoreCase("itemStatus")) {
                map.put("itemStatus", item.getItemStatus());
            }

        }

        return map;

    }
}
