/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.applaudo.ejbremote.services.impl;

import java.text.DecimalFormat;
import java.util.Collections;

import sv.com.applaudo.ejbremote.dao.ItemDAO;
import sv.com.applaudo.ejbremote.models.entities.Item;
import sv.com.applaudo.ejbremote.services.remotes.ItemBusinessBeanRemote;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import sv.com.applaudo.ejbremote.models.enums.EnumStatus;

/**
 *
 * @author stevo
 */
@Stateless(name = "ItemBusinessBeanRemote")
public class ItemBusinessBean implements ItemBusinessBeanRemote{
    
    private static final Logger LOGGER = Logger.getLogger(ItemBusinessBean.class.getName());
    
    @PersistenceContext
    private EntityManager entityManager;
    
    private static final DecimalFormat df = new DecimalFormat("#.#");

    @Override
    public List<Item> findAll() throws Exception {
        
        try{
            ItemDAO itemDAO = new ItemDAO(entityManager);
            return itemDAO.findAll();
        }
        catch(Exception e){
            throw e;
        }
    }

    @Override
    public Item save(Item item) throws Exception {
        try{
            ItemDAO itemDAO = new ItemDAO(entityManager);
            if(item.getItemBuyingPrice()!=null){
                item.setItemBuyingPrice(Double.parseDouble(df.format(item.getItemBuyingPrice())));
            }
            if(item.getItemSellingPrice()!=null){
                item.setItemSellingPrice(Double.parseDouble(df.format(item.getItemSellingPrice())));
            }

            return itemDAO.save(item);
        }
        catch(Exception e){
            throw e;
        }
    }

    @Override
    public List<Item> findByStatusAndName(String status, String name) throws Exception {
        try{
            ItemDAO itemDAO = new ItemDAO(entityManager);
            List<Item> list = itemDAO.findByStatusAndName(EnumStatus.findByValue(status), name);
            
            return list;
        }
        catch(Exception e){
            throw e;
        }
    }

    @Override
    public Item findById(Long id) throws Exception {
        if(id!=null){        
            try{
                ItemDAO itemDAO = new ItemDAO(entityManager);
                return itemDAO.findById(id);
            }
            catch(Exception e){
                throw e;
            }
        }
        
        return null;
    }
    
    @Override
    public Map<String, Object> findByIdWithParams(String fields, Long id) throws Exception {
        if(id!=null && fields!=null && fields.length()>0){        
            try{
                ItemDAO itemDAO = new ItemDAO(entityManager);
                
                return itemDAO.filterData(fields, id);
            }
            catch(Exception e){
                throw e;
            }
        }
        return Collections.EMPTY_MAP;
    }

    
    
}
