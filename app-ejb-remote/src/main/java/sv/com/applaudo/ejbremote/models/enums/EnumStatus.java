/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.applaudo.ejbremote.models.enums;

/**
 *
 * @author stevo
 */
public enum EnumStatus {
    
    AVAILABLE("Available"),
    SOLD("Sold");
    
    private final String text;


    private EnumStatus(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

    public String getText() {
        return text;
    }
    
    public static EnumStatus findByValue(String value) {
        EnumStatus result = null;
        for (EnumStatus s : values()) {
            if (s.getText().equalsIgnoreCase(value)) {
                result = s;
                break;
            }
        }
        return result;
    }
    
}
