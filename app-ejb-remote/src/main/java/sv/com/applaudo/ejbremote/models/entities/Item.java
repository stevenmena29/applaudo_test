/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.applaudo.ejbremote.models.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import sv.com.applaudo.ejbremote.models.enums.EnumStatus;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotEmpty;
import org.hibernate.annotations.CreationTimestamp;

/**
 *
 * @author stevo
 */
@Entity
@Table(name = "items")
@NamedQueries({
    @NamedQuery(name = "Item.findAll", query = "SELECT i FROM Item i")})
public class Item implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "item_id", nullable = false, unique = true)
    private Long itemId;
    
    @Column(name = "item_name",nullable = false)
    @NotEmpty
    private String itemName;
    
    @Column(name = "item_buying_price", precision=5, scale=1)
    private Double itemBuyingPrice;
    
    @Column(name = "item_selling_price",precision=5, scale=1)
    private Double itemSellingPrice;
    
    @Column(name = "item_entered_user", nullable = false)
    @NotEmpty
    private String itemEnteredByUser;
    
    @CreationTimestamp
    @Column(name = "item_entered_date", nullable = false)
    private LocalDateTime itemEnteredDate;

    @Column(name = "item_last_modified_date")
    private LocalDateTime itemLastModifiedDate;
    
    @Column(name = "item_last_modified_user")
    private String itemLastModifiedByUser;
    
    
    @Column(name = "item_status")
    @Enumerated(EnumType.STRING)
    private EnumStatus itemStatus;

    public Item() {
        this.itemStatus = EnumStatus.AVAILABLE;
    }
    
    @PreUpdate
    public void preUpdate() {
        itemLastModifiedDate = LocalDateTime.now();
    }
    

    
    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Double getItemBuyingPrice() {
        return itemBuyingPrice;
    }

    public void setItemBuyingPrice(Double itemBuyingPrice) {
        this.itemBuyingPrice = itemBuyingPrice;
    }

    public Double getItemSellingPrice() {
        return itemSellingPrice;
    }

    public void setItemSellingPrice(Double itemSellingPrice) {
        this.itemSellingPrice = itemSellingPrice;
    }

    public String getItemEnteredByUser() {
        return itemEnteredByUser;
    }

    public void setItemEnteredByUser(String itemEnteredByUser) {
        this.itemEnteredByUser = itemEnteredByUser;
    }

    public LocalDateTime getItemEnteredDate() {
        return itemEnteredDate;
    }

    public void setItemEnteredDate(LocalDateTime itemEnteredDate) {
        this.itemEnteredDate = itemEnteredDate;
    }

    public LocalDateTime getItemLastModifiedDate() {
        return itemLastModifiedDate;
    }

    public void setItemLastModifiedDate(LocalDateTime itemLastModifiedDate) {
        this.itemLastModifiedDate = itemLastModifiedDate;
    }

    public String getItemLastModifiedByUser() {
        return itemLastModifiedByUser;
    }

    public void setItemLastModifiedByUser(String itemLastModifiedByUser) {
        this.itemLastModifiedByUser = itemLastModifiedByUser;
    }

    public EnumStatus getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(EnumStatus itemStatus) {
        this.itemStatus = itemStatus;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.itemId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Item other = (Item) obj;
        if (!Objects.equals(this.itemId, other.itemId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Item{" + "itemId=" + itemId + ", itemName=" + itemName + ", itemStatus=" + itemStatus + '}';
    }

}
