/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.applaudo.ejbremote.services.remotes;

import sv.com.applaudo.ejbremote.models.entities.Item;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.ejb.Remote;

/**
 *
 * @author stevo
 */
@Remote
public interface ItemBusinessBeanRemote extends Serializable {
    
    
    List<Item> findAll() throws Exception;

    Item save(Item item) throws Exception;

    List<Item> findByStatusAndName(String status, String name) throws Exception;

    Item findById(Long id) throws Exception;
    
    Map<String, Object> findByIdWithParams(String fields, Long id) throws Exception; 

}
