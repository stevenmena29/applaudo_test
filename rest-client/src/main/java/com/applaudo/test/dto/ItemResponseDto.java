package com.applaudo.test.dto;


import lombok.*;

@NoArgsConstructor
@Data
public class ItemResponseDto {
 
    public ItemResponseDto setValues(String status, String message) {
        this.status = status;
        this.message = message;
        return this;
    }
    

    @Getter
    private static final String OK = "OK";
    @Getter
    private static final String MESSAGE_SAVE = "Item save successfully!";
    @Getter
    private static final String ERROR = "ERROR";
    @Getter
    private static final String MESSAGE_ERROR_SAVE = "Item can't save!";
    @Getter
    private static final String MESSAGE_UPD = "Item updated successfully!";
    @Getter
    private static final String MESSAGE_ERROR_FINDID = "Item doesn't exist!";
    @Getter
    private static final String MESSAGE_ERROR = "Error: check logs!";


    private String status;
    private String message;
}
