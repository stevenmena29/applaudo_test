package com.applaudo.test;


import java.util.Properties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import sv.com.applaudo.ejbremote.services.remotes.ItemBusinessBeanRemote;


@SpringBootApplication
public class TestApplication{

    @Bean   
    public Context context() throws NamingException {
        Properties jndiProps = new Properties();
        jndiProps.put("java.naming.factory.initial", "org.jboss.naming.remote.client.InitialContextFactory");
        jndiProps.put("jboss.naming.client.ejb.context", true);
        jndiProps.put("java.naming.provider.url", "http-remoting://localhost:8080");
        return new InitialContext(jndiProps);
    }

    @Bean
    public ItemBusinessBeanRemote itemBusinessBeanRemote(Context context) throws NamingException {
        return (ItemBusinessBeanRemote) context.lookup(this.getFullName(ItemBusinessBeanRemote.class));
    }

    
    @SuppressWarnings("rawtypes")
    private String getFullName(Class classType) {
        String moduleName = "app-ejb-remote/";
        String beanName = classType.getSimpleName();
        String viewClassName = classType.getName();
        
        return moduleName + beanName + "!" + viewClassName;
    }

    public static void main(String[] args) {

        SpringApplication.run(TestApplication.class, args);
    }

}
