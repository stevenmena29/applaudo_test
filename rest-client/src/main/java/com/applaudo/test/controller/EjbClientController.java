package com.applaudo.test.controller;



import com.applaudo.test.dto.ItemResponseDto;
import com.applaudo.test.utils.UserAuthenticated;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sv.com.applaudo.ejbremote.models.entities.Item;
import sv.com.applaudo.ejbremote.services.remotes.ItemBusinessBeanRemote;



@RestController
@RequestMapping("/v1/item")
@Validated
public class EjbClientController {

    private static final Logger logger = LoggerFactory.getLogger(EjbClientController.class);
    
    private ItemBusinessBeanRemote itemBusinessBeanRemote;
    
    @Autowired
    private UserAuthenticated authenticated;

    public EjbClientController(ItemBusinessBeanRemote itemBusinessBeanRemote) {
        this.itemBusinessBeanRemote = itemBusinessBeanRemote;
    }
    
    

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByNameAndStatus(@RequestParam(required = false) String status, @RequestParam(required = false) String name) {
        ItemResponseDto responseDto = new ItemResponseDto();
        try {
            List <Item> items = new ArrayList();
            if(name!=null && status!=null){
                items = itemBusinessBeanRemote.findByStatusAndName(status, name);
                return new ResponseEntity<>(items, HttpStatus.OK);
            }
            else if(name==null && status==null){
                items = itemBusinessBeanRemote.findAll();
                return new ResponseEntity<>(items, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>("Params status and name are mandatory to filter", HttpStatus.BAD_REQUEST);
            }
            
        } catch (Exception ex) {
           logger.error(ex.getMessage());
           responseDto.setValues(ItemResponseDto.getERROR(), ItemResponseDto.getMESSAGE_ERROR());
           return new ResponseEntity<>(responseDto, HttpStatus.BAD_REQUEST);
        }
    }
    
    @GetMapping(value="/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getById(@PathVariable Long id,@RequestParam(name = "fields", required=false) String fields) {
        ItemResponseDto responseDto = new ItemResponseDto();
        try {
            if(id!=null){
                if(fields!=null && fields.length()>0){
                    Map<String, Object> result = itemBusinessBeanRemote.findByIdWithParams(fields,id);
                    return new ResponseEntity<>(result, HttpStatus.OK);
                }
                else{
                    Item item = itemBusinessBeanRemote.findById(id);
                    if(item!=null){
                        return new ResponseEntity<>(item, HttpStatus.OK);
                    }
                    else{
                        responseDto.setValues(ItemResponseDto.getERROR(), ItemResponseDto.getMESSAGE_ERROR_FINDID());
                        return new ResponseEntity<>(responseDto, HttpStatus.NOT_FOUND);
                    }
                }
                
            }
            else{
                responseDto.setValues(ItemResponseDto.getERROR(), ItemResponseDto.getMESSAGE_ERROR());
                return new ResponseEntity<>(responseDto, HttpStatus.BAD_REQUEST);
            }
            
        } catch (Exception ex) {
           logger.error(ex.getMessage());
           responseDto.setValues(ItemResponseDto.getERROR(), ItemResponseDto.getMESSAGE_ERROR());
           return new ResponseEntity<>(responseDto, HttpStatus.BAD_REQUEST);
        }
    }
    
    @PostMapping(value="/",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> save(@Valid @RequestBody Item item){
        ItemResponseDto responseDto = new ItemResponseDto();
        try {
            item.setItemEnteredByUser(authenticated.getUserName());
            Item itemSave = itemSave=itemBusinessBeanRemote.save(item);
            return new ResponseEntity<>(itemSave, HttpStatus.CREATED);
        }
        catch (Exception e){
            logger.error(e.getMessage());
            responseDto.setValues(ItemResponseDto.getERROR(), ItemResponseDto.getMESSAGE_ERROR_SAVE());
            return new ResponseEntity<>(responseDto, HttpStatus.BAD_REQUEST);
        }
    }
    
}
